﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartGenerator : MonoBehaviour
{
    public GameObject HeartPrefab;
		float span = 10.0f;
		float delta = 0;

		public void SetSpan(float span){
				this.span = span;
		}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
				if(this.delta > this.span) {
						this.delta = 0;
						GameObject go = Instantiate(HeartPrefab) as GameObject;
						float px = Random.Range(-20.0f,20.0f);
						go.transform.position = new Vector3(px,60,0);
				}
    }
}
