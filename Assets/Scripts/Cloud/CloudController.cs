﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour
{

		// プレイヤー
		GameObject player;
			// 消滅時間
		float time = 1.0f;
			float px;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("Sepiroth");
				this.px = Random.Range(-0.10f,-0.03f);
    }

    // Update is called once per frame
    void Update()
    {
				
				Vector2 p1 = transform.position;							// 雲の中心座標
				Vector2 p2 = this.player.transform.position;  // プレイヤーの中心座標
				Vector2 dir = p1 - p2;
				float d = dir.magnitude;
				float r1 = 2.0f; // 索敵半径
				float r2 = 1.0f; // プレイヤーの半径
				
        // frameごとに等速で左に移動させる
				
				transform.Translate(px, 0, 0);

					if(d < r1 + r2){

						// プレイヤーの近くに2秒以上存在する場合、雲が消滅する。
						this.time -= Time.deltaTime;
						if(this.time < 0){
								Destroy(gameObject);
								// director.GetComponent<GameDirector>().EnemyHit();
								this.time = 0f;
						}
				}

				// 画面外に出たらオブジェクトを破棄する
				if(transform.position.x < -60.0f) {
					Destroy(gameObject);
				}

    }
}
