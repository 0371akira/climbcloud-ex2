﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudGenerator : MonoBehaviour
{

		public GameObject MoveCloudPrefab;
		float span = 0.5f;
		float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
				if(this.delta > this.span) {
						this.delta = 0;
						GameObject go = Instantiate(MoveCloudPrefab) as GameObject;
						int px = Random.Range(-10,60);
						go.transform.position = new Vector3(60,px,0);
				}
    }
}
