﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearController : MonoBehaviour
{

		GameObject player;
		GameObject director;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("Sepiroth");
				this.director = GameObject.Find("SEController");
    }

    // Update is called once per frame
    void Update()
    {
        // frameごとに等速で落下させる
				transform.Translate(0, -0.25f, 0);

				// 画面外に出たらオブジェクトを破棄する
				if(transform.position.y < -30.0f) {
					Destroy(gameObject);
				}

				// 当たり判定
				Vector2 p1 = transform.position;							// 矢の中心座標
				Vector2 p2 = this.player.transform.position;  // プレイヤーの中心座標
				Vector2 dir = p1 - p2;
				float d = dir.magnitude;
				float r1 = 0.5f; // 矢の半径
				float r2 = 1.0f; // プレイヤーの半径

				if(d < r1 + r2){
						// GameDirectorにプレイヤと衝突したことを伝える
						GameObject director = GameObject.Find("SEController");
						
						director.GetComponent<SEController>().DecreaseHp();
						
						// 衝突した場合はSEを鳴らし、槍を消す
						Destroy(gameObject);
						director.GetComponent<SEController>().SpearHit();
				}
    }
}
