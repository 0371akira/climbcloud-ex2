﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SEController : MonoBehaviour {

		GameObject SepirothGauge;
 
		public AudioClip HeartSE;
		public AudioClip SpearSE;
		public AudioClip EnemySE;
		public AudioClip GoalSE;
		AudioSource aud;

    // Start is called before the first frame update
    void Start()
    {
        this.aud = GetComponent<AudioSource>();
				this.SepirothGauge = GameObject.Find("Sepiroth-icon");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

		// 矢が当たった時にSEを鳴らす
		public void SpearHit(){
			this.aud.PlayOneShot(this.SpearSE);
			Debug.Log ("Spear");
		} 

		// 敵が当たったときにSEを鳴らす
		public void EnemyHit(){
			this.aud.PlayOneShot(this.EnemySE);
			Debug.Log ("Enemy");
		}

		// 回復したときにSEを鳴らす
		public void HeartHit(){
			this.aud.PlayOneShot(this.HeartSE);
			Debug.Log ("Heart");
		}  

	  // ダメージを受けたらHPゲージを減らす
		public void DecreaseHp() {
        this.SepirothGauge.GetComponent<Image>().fillAmount -= 0.1f;
		}

		// ダメージを受けたらHPゲージを減らす
		public void IncreaseHp() {
        this.SepirothGauge.GetComponent<Image>().fillAmount += 0.3f;
		}
}
