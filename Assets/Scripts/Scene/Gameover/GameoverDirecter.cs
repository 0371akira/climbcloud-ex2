﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameoverDirecter : MonoBehaviour
{

    // Update is called once per frame
    void Update()
     {
        if(Input.GetKeyDown(KeyCode.Space)) {
					SceneManager.LoadScene("GameScene");
				}
				if(Input.GetKeyDown(KeyCode.Escape)) {
					SceneManager.LoadScene("TitleScene");
				}
    }
}
