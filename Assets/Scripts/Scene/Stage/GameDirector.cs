﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{

		GameObject Sepiroth;
		GameObject SepirothGauge;
		GameObject SpearPrefab;

		GameObject timerText;
		float time = 97.0f;

    // Start is called before the first frame update
    void Start()
    {
				this.Sepiroth = GameObject.Find("Sepiroth");
        this.SepirothGauge = GameObject.Find("Sepiroth-icon");
				this.SpearPrefab = GameObject.Find("SpearPrefab");
				this.timerText = GameObject.Find("Timer");
    }

    // Update is called once per frame
    void Update()
    {
				this.time -= Time.deltaTime;
				this.timerText.GetComponent<Text>().text = this.time.ToString("F1");
				
				// プレイヤーが画面下に落ちるか、HPゲージが0になるか、タイマーが0のとき
        if(this.Sepiroth.transform.position.y < -30 ||
					 this.SepirothGauge.GetComponent<Image>().fillAmount == 0 ||
					 this.time <= 0 ) {

						// ゲームオーバーシーンに移行する
						SceneManager.LoadScene("GameoverScene");
				}
    }

}
