﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DescriptionDirector : MonoBehaviour
{
   
		GameObject Sepiroth;
		GameObject SepirothGauge;
		GameObject SpearPrefab;

    // Start is called before the first frame update
    void Start()
    {
				this.Sepiroth = GameObject.Find("Sepiroth");
        this.SepirothGauge = GameObject.Find("Sepiroth-icon");
				this.SpearPrefab = GameObject.Find("SpearPrefab");
    }

    // Update is called once per frame
    void Update()
    {
				// プレイヤーが画面下に落ちるか、HPゲージが0になったとき
        if(this.Sepiroth.transform.position.y < -10 || 
					 this.SepirothGauge.GetComponent<Image>().fillAmount == 0) {

						// 次のシーンに移行する
						SceneManager.LoadScene("Event1-2Scene");
				}
    }
}
