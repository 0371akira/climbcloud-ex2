﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class autoEnemy : MonoBehaviour{
 
    // 目的地に着いたかどうか
    private bool isReachTargetPosition;
    // 目的地
    private Vector3 targetPosition;
		// プレイヤー
		GameObject player;
		// 消滅時間
		float time = 3.0f;
		GameObject director;
 
    // x軸 下限
    public const float X_MIN_MOVE_RANGE = -40f;
    // x軸 上限
    public const float X_MAX_MOVE_RANGE = 40f;
    // y軸 下限
    public const float Y_MIN_MOVE_RANGE = -20f;
    // y軸 上限
    public const float Y_MAX_MOVE_RANGE = 20f;
    // 移動スピード
    public const float SPEED = 0.03f;
 
    void Start(){
        this.isReachTargetPosition = false;
        decideTargetPotision();
				this.player = GameObject.Find("Sepiroth");
				this.director = GameObject.Find("SEController");
    }
 
    void Update(){

				Vector2 p1 = transform.position;							// 矢の中心座標
				Vector2 p2 = this.player.transform.position;  // プレイヤーの中心座標
				Vector2 dir = p1 - p2;
				float d = dir.magnitude;
				float r1 = 6.0f; // 索敵半径
				float r2 = 1.0f; // プレイヤーの半径
				float r3 = 0.7f; // Enemyの当たり判定
				
				if(d < r1 + r2){
		        Vector3 playerPos = this.player.transform.position;
        		float ENEMY_MOVE_SPEED = 0.06f;
       			// プレイヤーの方向に移動させる
        		transform.position = Vector3.MoveTowards(transform.position, playerPos, ENEMY_MOVE_SPEED);

						// プレイヤーの近くに3秒以上存在する場合、Enemyが消滅する。
						this.time -= Time.deltaTime;
						if(this.time < 0){
								Destroy(gameObject);
								director.GetComponent<SEController>().EnemyHit();
								this.time = 0f;
						}
				} else {
						decideTargetPotision();
		        // 現在地から目的地までSPEEDの速度で移動する
		        transform.position = Vector3.MoveTowards(transform.position, targetPosition, SPEED);
		        // 目的地についたらisReachTargetPositionをtrueにする
		        if(transform.position == targetPosition){
		            isReachTargetPosition = true;
						}
				}

				if(d < r2 + r3){
						
						// GameDirectorにプレイヤと衝突したことを伝える
						
						
						director.GetComponent<SEController>().DecreaseHp();
						
						// 衝突した場合はSEを鳴らし、Enemyを消す
						Destroy(gameObject);
						director.GetComponent<SEController>().EnemyHit();
				}
				
    }
 
    // 目的地を設定する
    private void decideTargetPotision(){
        // まだ目的地についてなかったら（移動中なら）目的地を変えない
        if(!isReachTargetPosition){
            return;
        }
 
        // 目的地に着いていたら目的地を再設定する
        targetPosition = new Vector3(Random.Range(X_MIN_MOVE_RANGE, X_MAX_MOVE_RANGE), Random.Range(Y_MIN_MOVE_RANGE, Y_MAX_MOVE_RANGE), 0);
        isReachTargetPosition = false;
    }
}