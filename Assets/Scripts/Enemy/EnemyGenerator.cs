﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
   public GameObject EnemyPrefab;
		float span = 5.0f;
		float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
				if(this.delta > this.span) {
						this.delta = 0;
						GameObject go = Instantiate(EnemyPrefab) as GameObject;
						int px = Random.Range(-30,30);
						go.transform.position = new Vector3(px,px,0);
				}
    }
}
