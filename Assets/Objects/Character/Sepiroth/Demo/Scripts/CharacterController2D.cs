﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
	[SerializeField] private float m_JumpForce = 400f;							// プレイヤーがジャンプした時に加わる力の量。Amount of force added when the player jumps.
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;			// しゃがむ動作に適用される最大速度の量。1 = 100% Amount of maxSpeed applied to crouching movement. 1 = 100%
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// どれだけ動きを滑らかにするか How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// ジャンプしながらの操舵ができるかどうか。 Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;							// キャラクターに何が地に足がついているかを決めるマスク A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;							// プレーヤーが接地しているかどうかを確認するためのポジションマーク。 A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_CeilingCheck;							// 天井の有無を確認する位置のマーキング A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;				// しゃがむと無効化されるコライダー A collider that will be disabled when crouching

	const float k_GroundedRadius = .2f; // 接地しているかどうかを判定するためのオーバーラップ円の半径 Radius of the overlap circle to determine if grounded
	private bool m_Grounded;            // プレイヤーが接地しているかどうか。 Whether or not the player is grounded.
	const float k_CeilingRadius = .2f; // プレイヤーが立ち上がることができるかどうかを判断するためのオーバーラップサークルの半径 Radius of the overlap circle to determine if the player can stand up
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;  //  プレイヤーが現在どちらの方向を向いているかを判断するためのものです。 For determining which way the player is currently facing.
	private Vector3 m_Velocity = Vector3.zero;

	// 【追加】ジャンプする時のSE
	public AudioClip JumpSE;
	AudioSource aud;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	public BoolEvent OnCrouchEvent;
	private bool m_wasCrouching = false;

	// 【追加】AudioSourceの取得
	void Start()
    {
				this.aud = GetComponent<AudioSource>();
    }

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();
	}

	private void LateUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		// This can be done using layers instead but Sample Assets will not overwrite your project settings.

		// 接地チェック位置へのサークルキャストが地面に指定されたものにヒットした場合、そのプレイヤーは接地していることとなる
		// これはレイヤーを使って行うことができますが、サンプルアセットはプロジェクトの設定を上書きしません。

		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
	}


	public void Move(float move, bool crouch, bool jump)
	{
		// If crouching, check to see if the character can stand up
		// しゃがむ場合は、キャラクターが立ち上がることができるかどうかを確認してください。

		if (!crouch)
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			// キャラクターが天井で立ち上がることができない場合は、しゃがんだままにしておきます。

			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
			}
		}

		// only control the player if grounded or airControl is turned on
		// 地上またはairControlがオンになっている場合にのみ、プレイヤーを制御します。

		if (m_Grounded || m_AirControl)
		{

			// If crouching
			if (crouch)
			{
				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					OnCrouchEvent.Invoke(true);
				}

				// Reduce the speed by the crouchSpeed multiplier
				// クロウチスピードの乗数で速度を下げます。

				move *= m_CrouchSpeed;

				// Disable one of the colliders when crouching
				// しゃがむ時にコライダーのいずれかを無効にします。

				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = false;
			} else
			{
				// Enable the collider when not crouching
				// しゃがんでいないときにコライダーを有効にする

				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					OnCrouchEvent.Invoke(false);
				}
			}

			// Move the character by finding the target velocity
			// 目標速度を見つけてキャラクターを移動させる
			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);

			// And then smoothing it out and applying it to the character
			// そして、それを滑らかにして、キャラクターに適用します
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// If the input is moving the player right and the player is facing left...
			// 入力がプレイヤーを右に動かしていて、プレイヤーが左を向いている場合は...
			if (move > 0 && !m_FacingRight)
			{
				// ... flip the player.
				// ...プレイヤーを反転させます。
				Flip();
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			// それ以外の場合は、入力がプレイヤーを左に移動していて、プレイヤーが右を向いている場合...
			else if (move < 0 && m_FacingRight)
			{
				// ... flip the player.
				// ...プレイヤーを反転させます。
				Flip();
			}
		}
		// If the player should jump...
		// プレイヤーがジャンプする場合は...
		if (m_Grounded && jump)
		{
			// Add a vertical force to the player.
			// プレイヤーに垂直方向の力を加えます。
			m_Grounded = false;
			m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
			// 【追加】ジャンプするときにSEを鳴らす
			Debug.Log(this.JumpSE);
			this.aud.PlayOneShot(this.JumpSE, 0.5f);
		}
	}


	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		// プレイヤーの向きを切り替えます。
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		// プレイヤーのxローカルスケールに-1を乗算します。
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}